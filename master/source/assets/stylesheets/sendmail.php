<?php

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        //$name = strip_tags(trim($_POST["name"]));
				//$name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        // $message = trim($_POST["message"]);
        
        if( !filter_var($email, FILTER_VALIDATE_EMAIL)){
            http_response_code(400);
            exit;
        }
        // Check that data was sent to the mailer.
        // if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        //     // Set a 400 (bad request) response code and exit.
        //     http_response_code(400);
        //     echo "Oops! There was a problem with your submission. Please complete the form and try again.";
        //     exit;
        // }

        $response = $_POST;

        if( $response["controlfield"] == "events" ){
            //  // events contacts

            $recipient = "racing@worldebikeseries.com";
            $subject = 'Wes Ask Event Request';
        }else{
            //  // media contacts
            $subject = 'Wes Contact';
            $recipient = "info@worldebikeseries.com";
        }

        $message = "<html> <body bgcolor='#f4f6f6'>";
        $message .= "<table align='center' bgcolor='#f4f6f6' border='0' cellpadding='0' cellspacing='0' width='600'>";
        $message .= "<tr bgcolor='white'><td width='20'>&nbsp;</td> <td> <p style='padding: 9px 0; text-align: center'> <img src='http://www.worldebikeseries.com/images/wes-logo-blue.svg' width='90' height='35' alt='Wes logo blue' /> </p> </td> <td width='20'>&nbsp;</td> </tr>";
        $message .= "<tr> <td width='20'>&nbsp;</td> <td> <p style='font-weight: 300; text-align: center; padding: 9px'> <font color='#2ab3d7' face='Arial, Helvetica, sans-serif' size='6'>" . $title . "</font> </p>";

        foreach ( $response as $key => $attribute ) {
            $message .= "<p style='line-height:1.5'>";
            $message .= "<font color='#00145e' face='Arial, Helvetica, sans-serif' size='3'><b>" . $key . "</b>: ";

            if ($key == "BIRTHDAY"){
                $val = "";
                $index = 0;
                foreach ( $attribute as $value ) {
                    $index = $index + 1;
                    if ( $index > 1){
                        $val .= "/" . $value;
                    }else{
                        $val .= $value;
                    }
                }
                $message .= $val;

            }else{
                $message .= $attribute;
            }

            $message .= "</font></p>";
        }
        $message .= "</td> <td width='20'>&nbsp;</td> </tr></table></body></html>";
        
        // Set the recipient email address.
        // FIXME: Update this to your desired email address.
        // Set the email subject.

        // Build the email content.
        
        $email_content = $message;

        // Build the email headers.
        $email_headers = "From: " . $name . " " .$email . "\r\n";
        $email_headers .= "Reply-To: ". $email . "\r\n";
        $email_headers .= "MIME-Version: 1.0\r\n";
        $email_headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            // header('Content-type: application/json');
            //echo json_encode( $data );

            header('Access-Control-Allow-Origin: http://www.worldebikeseries.com/');
            header('Content-type: application/json');
                $response = array();
                $response[0] = array(
                    'success' => true,
                    'response'=> 'Thank You! Your message has been sent.'
                );

            echo json_encode($response); 


        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);

            header('Access-Control-Allow-Origin: http://www.worldebikeseries.com/');
            header('Content-type: application/json');
                $response = array();
                $response[0] = array(
                    'success' => false,
                    'response'=> 'Oops! Something went wrong and we couldn \'t send your message.'
                );

            echo json_encode($response); 
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        
        header('Access-Control-Allow-Origin: http://www.worldebikeseries.com/');
            header('Content-type: application/json');
                $response = array();
                $response[0] = array(
                    'success' => false,
                    'response'=> 'There was a problem with your submission, please try again.'
                );

            echo json_encode($response); 
    }

?>