
  window.theme = window.theme || {};

  AOS.init({
    once: true
  });
  
  window.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
  };

  theme.Carousel = (function(){

    this.$window = $(window);
    this.$carousel = $('[data-js-carousel]');
    this.$items = $carousel.find('[data-js-carousel-item]');
    this.$handler = $items.find('button');


    var _someItemIsOpen = false;
    var activeClass = 'js-open';
    /** events */
    if($carousel.length){
      console.log('theme says: \'carousel start\' ', $carousel);

      $window.on('resize', _.throttle(setItem, 25) );
      $window.on('load', _.throttle(InitCarousel, 25) );

      if( window.outerWidth > 768 && this.$items.length <= 3 ){
        console.log('ui')
        $carousel.addClass('no-button');
      }


      // setWidth(null, $items);

      // $carousel.addClass('js-loading');
    
      // $carousel.flickity({
      //   freeScroll: true,
      //   contain: true,
      //   prevNextButtons: false,
      //   pageDots: false,
      //   cellAlign: 'left'
      // });



      // $carousel.on( 'ready.flickity', onInit );
      // $carousel.on( 'staticClick.flickity', onStaticClick );

    }

    function InitCarousel() {

      $carousel.on('init', () => {
        $carousel.on('click', '.slick-slide button', $.proxy( toggleItem , this) )
        $items = $carousel.find('.slick-slide');

        setItem();
        // setHeight( $items );
      });

      $carousel.on('afterChange', (e, c, currentSlide) => {
        // control index
        $items = $carousel.find('.slick-slide');

        if(_someItemIsOpen){
          const _currentSlide$ = $items.eq(currentSlide).find('button');
          // _currentSlide$.trigger('click');

        }

        // isOpen

        // setItem();
        // setHeight( $items );
        // console.log(event, currentSlide, end);
        
      });


      


      $carousel.slick({
        mobileFirst: true,
        infinite: false,
        slidesToShow: 1,
        initialSlide: 0,
        slidesToScroll: 1,
        focusOnSelect: false,
        variableWidth: true,
        nextArrow: '<button class="z-30 top-6 right-0 border-0 background-none absolute arrow-next"><svg viewBox="0 0 25 45" width="25" height="45" xmlns="http://www.w3.org/2000/svg"> <g fill="none" fill-rule="evenodd"><path d="M1 1l22 21.5L1 44" stroke="currentColor" stroke-width="2"/></g> </svg></button>',
        prevArrow: '<button class="z-30 top-6 left-0 border-0 background-none absolute arrow-prev"><svg viewBox="0 0 25 45" width="25" height="45" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path d="M24 44L2 22.5 24 1" stroke="currentColor" stroke-width="2"/></g></svg></button>',
        responsive: [{
          breakpoint: 768,
          settings: {
            initialSlide: 0,
            slidesToShow: 2
          }
        }]
      })
    }

    function setItem(_item) {

      var _w = $carousel.width();
      var _m = window.outerWidth > 768 ? 3 : 1;
      var _i = window.outerWidth > 768 ? 2 : 1;
      var _mW = parseInt( (_w / _m), 10);

      $items.width(_mW);

      if(_item) {
        if( window.outerWidth > 768 && this.$items.length <= 3  ){
          $carousel.removeClass('no-button');
        }
      }else{
        if( window.outerWidth > 768 && this.$items.length <= 3  ){
          $carousel.addClass('no-button');
        }
      }

      if( _item && !( _item.hasOwnProperty('type') ) ) {
        _item.width(_mW * _i);
      }

      if( _item && ( _item.hasOwnProperty('type') && _item.type === 'resize') ) {
        var _current = $carousel.find('.js-open');
        $items.not(_current).width(_mW);
        _current.width(_mW * _i);
      }

    }

    function toggleItem (event) {

      var _notMobile = window.outerWidth > 768;
      var $currentParent = $(event.currentTarget).parents('.slick-slide');
      var _currentIndex = Number($currentParent.attr('data-slick-index'));
      var _currentSlideIndex = this.$carousel.slick('slickCurrentSlide');
      var _currentSlide = this.$items.eq( _currentSlideIndex );
      var _clickedSlide = this.$items.eq( _currentIndex );
      
      var _clienRect = $currentParent.get(0).getBoundingClientRect();

      var offsetPositionRight = ((window.outerWidth / 2) < _clienRect.right);

      // console.log({current:_clickedSlide, item:$carousel, currentSlide:_currentSlide, offsetPositionRight: offsetPositionRight});

      console.log({_currentSlideIndex: _currentSlideIndex, _currentIndex: _currentIndex})

      // this.$carousel.slick('slickGoTo', _currentIndex, true);

      if( _clickedSlide.hasClass('js-open') ) {
        _someItemIsOpen = false;
        this.$items.removeClass('js-open')
        if( _clickedSlide.hasClass('js-repositionate') && _notMobile ){
          this.$carousel.slick('slickGoTo', _currentIndex-2, false);
        }


          $('html, body').animate({
            'scrollTop': $carousel.offset().top + 'px'
          })
        

        setItem();
      }else{
        _someItemIsOpen = true;
        this.$items.removeClass('js-open')
        _clickedSlide.addClass('js-open js-repositionate');
        setItem(_clickedSlide);
        if(offsetPositionRight && _notMobile){
          this.$carousel.slick('slickGoTo', _currentIndex-1, false);
        }
        else{
          this.$carousel.slick('slickGoTo', _currentIndex, false);
        }
      }

    }

  })();

  theme.Map = (function(){
    console.log('theme says: \'map start\' ')
    // <section data-js-map>
		// 	<div data-js-map-navigation>
		// 		<button data-js-map-items="partners">Insight Partners</button>
		// 		<button data-js-map-items="collaborative">Insight Collaborative</button>
		// 		<button data-js-map-items="both">Both</button>
		// 	</div>
		// 	<div data-js-pins>
		// 		<span data-js-pin="partners">
		// 			<span class="rounded w-9 h-9 inline-block"></span>
		// 			<div data-js-pin-tooltip>tooltip</div>
		// 		</span>
		// 		<span data-js-pin="collaborative">
		// 			<span class="rounded w-9 h-9 inline-block"></span>
		// 			<div data-js-pin-tooltip>tooltip</div>
		// 		</span>
		// 		<span data-js-pin="both">
		// 			<span class="rounded w-9 h-9 inline-block"></span>
		// 			<div data-js-pin-tooltip>tooltip</div>
		// 		</span>
		// 	</div>
		// </section>
  })();

  theme.inputs = (function(){
    console.log('theme says: \'inputs start\' ')
    $('input, textarea').on( 'change', function(event) {
      if( $(this).val().trim() !== ''){
        $(this).parent('label').addClass('is-filled');
      }else{
        $(this).parent('label').removeClass('is-filled');
      }
    })
  })();

  theme.parallax = (function(){

    if( $('.parallax-image--left, .parallax-image--right').length ){

      var image = document.getElementsByClassName('parallax-image--left');
      new simpleParallax(image, {
        overflow: true
      });

      var image = document.getElementsByClassName('parallax-image--right');
      new simpleParallax(image, {
        overflow: true
      });
    }

  })()

  theme.sendForm = (function(){ 

    var FORM = $('form');
    var MESSAGE = FORM.find('.message');

    MESSAGE.hide();

    if(FORM.length) {

      FORM.on('submit', sendForm);

    }

    function showMessage(message, type) {
      MESSAGE.show();
      MESSAGE.addClass('is-',type);
      MESSAGE.text(message);
    }

    function hideMessage() {
      MESSAGE.show();
    }

    function sendForm(event){
      $.ajax( {
        url: FORM.attr('action'),
        method: FORM.attr('method'),
        data: FORM.serialize()
      }).then(function(valid) {
        showMessage(valid, 'submitted');
      }, function(error){
        hideMessage(error, 'error');
        console.log({error: error})
      });
      event.preventDefault()
    }

  })();


  theme.accordion = (function(){

    console.log('theme says: \'accordion start\' ')

    var accordion$ = $('[data-js-accordion]');
    var accordionitems$ = accordion$.find('[data-js-accordion-item]');
    var accordionhandlers$ = accordion$.find('[data-js-accordion-button]');

    $(window).on('load', findAndOpen);
    accordion$.addClass('accordion-js-init');
    accordionhandlers$.on('click', findAndOpen);

    window.onpopstate = function(ev) {
      console.log(ev)
;    }

    function removeHash () { 
        history.pushState("", document.title, window.location.pathname + window.location.search);
    }

    function findAndOpen(event) {
      var _CURRENTTARGET = $(event.currentTarget).parents('[data-js-accordion-item]');
      var _ID = _CURRENTTARGET.attr('id') ? '#' + _CURRENTTARGET.attr('id') : undefined;
      var hasHash = !!window.location.hash;

      if( hasHash && _ID ){

        if( window.location.hash === _ID){
          removeHash();
          _CURRENTTARGET.removeClass('js-open');
        }else{
          history.pushState({}, null, _ID);
          _CURRENTTARGET.addClass('js-open');
          _CURRENTTARGET.siblings().removeClass('js-open');
        }
        
      } else if(hasHash) {
        accordion$.find(window.location.hash).addClass('js-open');

      }else{

        if(_ID){
          history.pushState({}, null, _ID);
          if (event.type === 'click') {
            _CURRENTTARGET.addClass('js-open');
            _CURRENTTARGET.siblings().removeClass('js-open');
          }
        }else{}
      }

      AOS.refresh();
      
    }

  })()
  theme.fileName = (function(){
  
    $('[type="file"]').each( function(){
      $(this).on('change', function(event) {
        $label = $(this).parents('label');
        if( event.target.files.length ){
          var _files = event.target.files;
          var content = '';

          for(var i = 0; i < _files.length; i++) {
            content += _files[i].name;
          }

          $label.attr('data-filecontent',content)
        }else{
          $label.attr('data-filecontent')
        }
        

      })
    })
  })()
  theme.wordsCount = (function(){

    $('[js-data-count]').each(function(){
      $(this).on('keyup', function (event) {
        const wordscount = $(event.target).attr('js-data-count');
        const minlimit = 100;
        const values = event.target.value;
        const result = values.match(/\S+\s*/g);

        $(this).next('.word-counter').text( result.length + '\/' + wordscount )
        if (result.length > wordscount) {
          $(this).addClass('has-herror');
        }
        if( result.length < minlimit) {
          console.log();
        }
      })
    })
  })();


  theme.toggler = (function(){

    $('[js-expand-toggle]').each( function() {
      $(this).on('click', function() {
        $destination = $( $(this).attr('js-expand-toggle') ).toggleClass("is-expandend");
        $(this).toggleClass("js-open");
      })
    })

  })()
  

  theme.navigation = (function(){

    console.log('theme says: \'navigation start\' ')

    var btn$ = $('.toggle-btn');
    var body$ = $('body');
    var hb$ = btn$.find('.js-hamburger');
    var submenu = $('.sub-item');
    var header$ = $('body > header')
    var nav$ = header$.find('nav');

    var $window = $(window);
    var $body =  $('body');

    if(window.mobilecheck()){
      $body.addClass('is-mobile');
    }

    var lastScrollValue = 0;

    setSubItem();

    $window.on('resize', _.throttle(setSubItem, 25) );

    $window.on('scroll', _.throttle(checkScroll, 25) );

    var subItemsBtn = $('.menu-item').filter( function( item ) {
      return $(this).find('.sub-menu').length !== 0
    } );

    console.log(subItemsBtn)

    subItemsBtn.addClass('has-subitem');



    btn$.on('click', function(){
      var context$ = $(this).parents('header');
      hb$.toggleClass('is-active');

      setTimeout(() => {

        body$.toggleClass('js-main-navigation-open');
        context$.toggleClass('js-navigation-open');  

        if (body$.hasClass('js-main-navigation-open') ){
          nav$.height(window.outerHeight - header$.outerHeight )
        }

        AOS.refresh();
        
      }, 25);




    })

    subItemsBtn.on('click', function(event){
      event.preventDefault();
      var context$ = $(this).parents('header');
      var btn = $(event.currentTarget);
      if( ! btn.hasClass('js-open-subitem') ){
        context$.find('a').removeClass('js-open-subitem');
        btn.addClass('js-open-subitem');
        body$.addClass('js-open-submenu')
      }else{
        context$.find('a').removeClass('js-open-subitem');
        body$.removeClass('js-open-submenu')
      }
      
        
    })

  function setSubItem(){

    var left = Math.floor( (window.outerWidth - $('header > div').width() ) / 2 );
    submenu.css('left', left * -1);
    submenu.css('left', 0);
  }

  function checkScroll() {

    if(!$body.hasClass('js-main-navigation-open')){
      if( $window.scrollTop() > 91) {
        $body.addClass('js-header-fixed');
      }else{
          $body.removeClass('js-header-fixed');
        
      }
      if (lastScrollValue > $window.scrollTop()) {
        $body.addClass('js-scroll-up');
      }else{
        $body.removeClass('js-scroll-up');
        
      }
    }
    lastScrollValue = $window.scrollTop();
  }

  })()

