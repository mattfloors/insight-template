<?php $suffix = esc_url( home_url( '/' ) ) . "/tag/" ?>
<section class="bg-blue-400 py-2 white p-gutter">
<h2 class="text-caption uppercase text-center m-0 mb-2">
insight fellows
</h2>
<div class="container mx-auto flex flex-wrap">
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2018-2019</h3>
<a class="text-c block white lh-2" href="#">Lorem ipsum</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2017-2018</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>elise-willer/">Elise Willer</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2015-2016</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>nicole-carli/">Nicole Carli</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2013-2014</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>kartik-madiraju/">Kartik Madiraju</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2009-2010</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>roxanne-krystalli">Roxanne Krystalli</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2008 - 2009</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>carl-conradi">Carl Conradi</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>carrie-stefansky">Carrie Stefansky</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>michelle-kissenkotter">Michelle Kissenkotter</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2007 - 2008</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>rebecca-brubaker">Rebecca Brubaker</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>seisei-tatebe-goddu">Seisei Tatebe-Goddu</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>eve-de-la-mothe-karoubi">Eve de la Mothe Karoubi</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2006 - 2007</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>victoria-fram">Victoria Fram</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>jared-leiderman">Jared Leiderman</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>julia-gegenheimer">Julia Gegenheimer</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2005 - 2006</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>dan-green">Dan Green</a>
</div>
</div>
</div>
<h2 class="text-caption uppercase text-center m-0 mb-2">
summers fellow
</h2>
<div class="container mx-auto flex flex-wrap">
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2008</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>holly-dranginis">Holly Dranginis</a>
</div>
</div>
</div>
</section>

<section class="bg-white black px-2 sm:px-0 text-center">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init aos-animate" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<h4 class="uppercase text-caption m-0">Support us</h4>
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6">Help us reach underserved groups and communities with the tools they need to thrive.</p>
<a class="blue-400 border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase" href="https://donorbox.org/support-insight-collaborative" target="_blank">
<span>make a donation</span>
</a>
</div>
</section>