
<?php 

	$accordion_excerpt = get_field(accordion_excerpt);
	$accordion_title = get_field(accordion_title);
	$accordion_content = get_field(accordion_content);
	$accordion_image = get_field(accordion_image);
	$accordion_id = get_field(accordion_id);
?>
<div class="border-b-2 border-blue-400" data-js-accordion-item="" id="item-0">
	<h2 class="flex justify-between">
		<span class="serif">What does the Fellowship year involve?</span>
		<button class="ml-auto border-0 background-none" type="button"><span class="active:arrow-rotate--big mr-1"></span></button>
	</h2>
	<?php ?>
	<div data-js-accordion-content="">
		<div class="text-c pt-0 pb-1 sm:w-10/12">
			The first three-month placement is held in Concord, Massachusetts at the offices of Insight Collaborative and its sister organisation, Insight Partners. During this placement, Fellows undergo rigorous training and coaching in communication, negotiation and mediation theory and skills. Fellows put learning into practice by observing and assisting Insight Partners consultants, and also gain access to Insight’s network of academics and practitioners. During this period, Fellows often volunteer to support local conflict management organizations, and pursue additional training they may need for their placements, such as language lessons. The remaining nine months typically include three foreign placements which the Fellows design and arrange during their Concord placement, with guidance from Insight Collaborative. Each Fellow identifies a broad theme upon which they will focus during their Fellowship, such as conflict and the environment, or methods of creating trust. Within a month of their return from their final placement, Fellows submit a written paper and provide a presentation on their Fellowship experience to Insight Collaborative. Each fellow is expected to submit journal entries to the Insight Collaborative blog during their placements.
		</div>
	</div>
</div>

<div id="grouptraining" data-js-accordion-item  class="flex border-t-2 blue-400 py-2">
	<?php if($accordion_image) : ?>
		<div class="w-full sm:w-2/12 pr-1 sm:pr-2">
			<figure class="h-3 w-3 relative sm:w-full sm:h-auto">
				<img src="<?php echo $accordion_image; ?>" class="w-full inset-center">
			</figure>
		</div>
	<?php endif ?>
	<div class="w-2/3 sm:w-10/12">
		<h2 class="flex text-h4 m-0 mb-1 serif justify-between">
			<?php echo $accordion_title; ?>
			<button class="ml-auto border-0 background-none" type="button"><span class=" active:arrow-rotate--big ic-asset mr-1"></span></button></h2>
		<?php if($accordion_excerpt) : ?>
			<div class="sm:w-3/4 text-c grey-700">
				<?php echo $accordion_excerpt; ?>
			</div>
		<?php endif ?>
		<div data-js-accordion-content class="grey-700 text-c w-10/12">
			<?php echo $accordion_content; ?>
		</div>
	</div>
</div>