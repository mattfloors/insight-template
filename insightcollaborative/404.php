<?php get_header(); ?>

<style>
	.text-diasply-h1{

		font-style: normal;
		font-weight: normal;
		font-size: 150px;
		line-height: 150px;

		letter-spacing: 0.15em;
	}
	.black{
		color: inherit;
	}
</style>

<section style="background-image: url(/img/bg-hp-what-we-do.jpg);" class="background-cover">
  <div class="container mx-auto py-4 sm:py-8 pt-6 sm:pt-8 text-center">
	<h1 class="text-diasply-h1 serif m-0 mb-2">404</h1>
	<p class="text-h3 serif mb-2">
		Sorry the page you’re looking<br> for cannot be found.
	</p>
	<p>

		<a class="border-2 flex-inline items-center lh-3 ls-custom no-underline px-2 space-no-wrap text-c uppercase black" href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank">
		<span>BACK TO THE MAIN SITE</span>
		</a>
	</p>
  </div>
</section>
<?php get_footer(); ?>