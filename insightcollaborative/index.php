<?php get_header(); ?>

<?php 
  




$project_sub_title = get_field('project_sub_title', '15');
$project_title = get_field('project_title', '15');
$project_content = get_field('project_content', '15');


$project_content_more = get_field('project_content_more', '15');
$project_images = get_field('project_images', '15');






?>

<section style="background-image: url(/img/bg-hp-what-we-do.jpg);" class="background-cover">
  <div class="container mx-auto py-2 sm:py-8 pt-6 sm:pt-8 flex flex-wrap">
    <figure class="w-full sm:w-1/2 p-gutter relative sm:order-last mb-2 aos-init aos-animate" data-aos-duration="2000" data-aos="fade-up" data-aos-delay="500">
      <img src="/img/PEP-FRONT.jpg" class="z-20 responsive relative">
      <img src="/img/PEP-BACK.jpg" class="z-10 absolute responsive parallax-image--left" style="transition: transform 0.4s cubic-bezier(0, 0, 0, 1) 0s; will-change: transform; transform: translate3d(0px, 4px, 0px);">
    </figure>
    <div class="sm:w-5/12 p-gutter sm:mr-auto sm:order-first aos-init aos-animate" data-aos-duration="2500" data-aos="fade-up" data-aos-delay="250">
      <h2 class="uppercase text-caption m-1 mb-2"><?php echo $project_title; ?></h2>
      <p class="text-h3 serif"><?php echo $project_sub_title; ?></p>
      <div class="text-p">
        <p><?php echo $project_content; ?></p>
        <div class="is-hidden to-reveal">
          <?php echo $project_content_more; ?>

        </div>
        <button js-expand-toggle=".to-reveal" type="button" class="ic-theme py-1 border-0 background-none p-0 blue-400 flex-inline items-center text-c mt-auto blue-400 font-medium">Read&nbsp;<span js-close="">more</span><span js-open="">less</span> <span class="active:arrow-rotate"></span></button>

      </div>
    </div>
  
  </div>
</section>
<section class="py-3 sm:py-4">
  <div class="container mx-auto">
  <h2 class="uppercase text-caption m-0 mb-2 w-full text-center">Other Insight Collaborative projects</h2>
  

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<article class="flex justify-center">
    <div class="pl-2 relative border-t-2 border-b-2 h-full py-1 border-blue-400 flex flex-column items-start sm:w-2/3">
      <p class="text-c">
        <span class="font-medium">The Radio Project</span> <br>by Peter Richardson
      </p>

      <div aria-hidden="true">
        <div class="content text-p pt-1 sm:w-10/12">
          <blockquote class="text-blockquote m-0 mb-2">Interactive Radio for Justice (IRfJ) encouraged dialogue between communities targeted by investigations of the International Criminal Court (ICC) and the national and international authorities who were responsible for bringing justice to these populations.</blockquote>
          <p>
            Working mainly in the Democratic Republic of Congo (DRC) and the Central African Republic (CAR), IRfJ used interactive local-language radio programming to trigger discussions about the purpose and impact of justice across communities who had suffered human rights abuses.
          </p>
          <p class="text-c mb-1">The project was led by Wamda Hall and supported by the MacArthur Foundation and Humanity United.</p>
        </div>
      </div>
    </div>
  </article>


<!--<?php get_template_part( 'entry' ); ?>-->
<?php comments_template(); ?>
<?php endwhile; endif; ?>
</div></section>
<section class="bg-white black px-2 sm:px-0 text-center">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init aos-animate" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<h4 class="uppercase text-caption m-0">Support us</h4>
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6">Help us reach underserved groups and communities with the tools they need to thrive.</p>
<a class="blue-400 border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase" href="https://donorbox.org/support-insight-collaborative" target="_blank">
<span>make a donation</span>
</a>
</div>
</section>
<section class="bg-green-400 px-2 sm:px-0 text-center white">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init aos-animate" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<img alt="Insight Collaborative Online" class="responsive" src="/assets/IP_LOGO_MAIN_NEGATIVE.svg">
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6">Our sister organization Insight Partners is a global leadership advisory firm, challenging exceptional individuals and teams to be their best.</p>
<a class="border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase white" href="/inside_partner/" target="_blank">
<span>VISIT the webSITE</span>
</a>
</div>
</section>
</main>
<?php get_footer(); ?>