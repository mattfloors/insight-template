</main>

<?php $theme_color = get_field('theme_color', 'option'); ?>
<?php $theme_footer = get_field('footer_logo', 'option'); ?>
<?php $theme_email = get_field('theme_email', 'option'); ?>
<?php $theme_phone = get_field('theme_phone', 'option'); ?>


<footer class='bg-<?php echo $theme_color ?>-400 white text-center sm:text-left text-footer border-t-2'>
	<div class='container mx-auto flex flex-wrap py-3'>
		<div class="w-full sm:w-5/12">
		  <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $theme_footer; ?>" alt="Insight Partners Online Logo" class="responsive"></a>
		</div>
		<div class="w-full sm:w-3/12 lh-2 relative my-2 sm:my-0">
		  <span class="separator-horizontal"></span>
		  	<?php if($theme_email) : ?>
		  		<a class="white no-underline" href="mailto:<?php echo $theme_email; ?>"><?php echo $theme_email; ?></a>
			<?php endif;?>
		  <br>
			<?php if($theme_phone) : ?>
				<span class="no-stylize"><?php echo $theme_phone; ?></span>
			<?php endif;?>
		  <br>
		  <a class="white no-underline inline-block mt-12" href="https://www.linkedin.com/company/insight-partners-online/about/" target="_blank" title="insight partners online linkedin profile"><svg width="45" height="45" viewBox="0 0 60 60" xmlns="http://www.w3.org/2000/svg"><path d="M0 30C0 13.431457 13.431457 0 30 0s30 13.431457 30 30-13.431457 30-30 30S0 46.568543 0 30zm21.200678-5.15269H14.40202v20.42714h6.798657V24.84731zm.447776-6.318938C21.604328 16.525504 20.172067 15 17.84629 15S14 16.525504 14 18.528372c0 1.961382 1.475572 3.530813 3.758038 3.530813h.043447c2.370582 0 3.84697-1.569431 3.84697-3.530813zm24.070778 15.03378c0-6.274333-3.35398-9.19451-7.82781-9.19451-3.609638 0-5.225735 1.982667-6.128076 3.373544v-2.893333h-6.799471c.08961 1.916777 0 20.427139 0 20.427139h6.79947v-11.4082c0-.6105.044127-1.219508.223889-1.656606.49136-1.219644 1.610122-2.482402 3.488394-2.482402 2.461143 0 3.444948 1.873122 3.444948 4.618135v10.92853h6.798385l.000271-11.712297z" fill="#FFF" fill-rule="evenodd" opacity=".5"/></svg></a>
		</div>
		
		<nav class="flex flex-column ml-auto w-full sm:w-2/12 lh-2 relative items-center sm:items-start">

		<span class='separator-horizontal'></span>
		<?php
		wp_nav_menu( array( 
			'menu_class' => '',
		    'theme_location' => 'footer-menu',
		    'container' => '',
		    'depth' => 1 ) ); 
		?>

		</nav>
	</div>
</footer>


</div>
<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/javascripts/lodash.min.js?t=29487573" defer=""></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/javascripts/init.js?t=2928273" defer=""></script>
</body>
</html>