<?php get_header(); ?>


<section class="bg-white">
<div class="container mx-auto py-3 pt-6 sm:py-4 sm:pt-8 flex flex-wrap justify-center">
<h3 class="uppercase text-caption mb-2 text-center w-full font-bold">
<strong class="font-medium">articles by <?php single_term_title(); ?></strong>
</h3>
<div class="article-grid">

<?php $index = 0; ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


	<?php if( $index == 0) : ?>


		<article class="border-1 border-blue-400 flex flex-column flex-start mb-1 p-1">
			<time>
				<?php get_post_time(); ?>
			</time>
			<h2 class="serif text-h4 lh-2">
			<a class="no-underline blue-400 flex align-center" href="<?php the_permalink(); ?>"><?php the_title(); ?>
			</a></h2>
			<div class="sm:text-blog">
				<?php the_excerpt(); ?>
			</div>
			<div class="background-cover" style="flex-grow: 1; min-height: 45vw; background-image: url(<?php echo get_the_post_thumbnail_url();?>)"></div>
		</article>

	<?php else: ?>

		<?php $attributeDataHidden = boolval($index > 2) ; ?> 

		<?php if ($attributeDataHidden) : ?>
			<article class="border-1 p-1 border-blue-400 flex flex-column flex-start mb-1" data-js-show-more>
		<?php else: ?>
			<article class="border-1 p-1 border-blue-400 flex flex-column flex-start mb-1">
		<?php endif; ?>
		<time>
			<?php the_date(); ?>
			<?php the_tags('') ?>
		</time>
		<h2 class="serif text-h4 lh-3">
		<a class="no-underline blue-400 flex align-center" href="<?php the_permalink(); ?>"><?php the_title(); ?>
		</a></h2>
		<div>
			<?php the_excerpt(); ?>
		</div>
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="background-cover after-padding-66" style="flex-grow: 1; background-image: url(<?php echo get_the_post_thumbnail_url();?>)"></div>
		<?php endif; ?>

		</article>
	<?php endif; ?>


	<?php $index = $index + 1; ?>


<?php endwhile; endif; ?>
<?php $suffix = esc_url( home_url( '/' ) ) . "/tag/" ?>
</div>
<p class="text-center"><button type="button" revealMorePost class="border-0 background-none blue-400 font-medium flex-inline items-center p-0">Show more articles <object class="ml-8px" data="/assets/arrow-blue.svg" type="image/svg+xml"></object> </button></p>
</div>
</section>

<section class="bg-blue-400 py-2 white p-gutter">
<h2 class="text-caption uppercase text-center m-0 mb-2">
insight fellows
</h2>
<div class="container mx-auto flex flex-wrap">

<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2017-2018</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>elise-willer/">Elise Willer</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2015-2016</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>nicole-carli/">Nicole Carli</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2013-2014</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>kartik-madiraju/">Kartik Madiraju</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2009-2010</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>roxanne-krystalli">Roxanne Krystalli</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2008 - 2009</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>carl-conradi">Carl Conradi</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>carrie-stefansky">Carrie Stefansky</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>michelle-kissenkotter">Michelle Kissenkotter</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2007 - 2008</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>rebecca-brubaker">Rebecca Brubaker</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>seisei-tatebe-goddu">Seisei Tatebe-Goddu</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>eve-de-la-mothe-karoubi">Eve de la Mothe Karoubi</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2006 - 2007</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>victoria-fram">Victoria Fram</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>jared-leiderman">Jared Leiderman</a>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>julia-gegenheimer">Julia Gegenheimer</a>
</div>
</div>
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2005 - 2006</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>dan-green">Dan Green</a>
</div>
</div>
</div>
<h2 class="text-caption uppercase text-center m-0 mb-2">
summers fellow
</h2>
<div class="container mx-auto flex flex-wrap">
<div class="p-gutter white w-1/2 sm:w-1/4">
<div class="border-t-1 py-1 pb-3">
<h3 class="serif text-h4">2008</h3>
<a class="text-c block white lh-2" href="<?php echo $suffix; ?>holly-dranginis">Holly Dranginis</a>
</div>
</div>
</div>
</section>

<section class="bg-white black px-2 sm:px-0 text-center">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init aos-animate" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<h4 class="uppercase text-caption m-0">Support us</h4>
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6">Help us reach underserved groups and communities with the tools they need to thrive.</p>
<a class="blue-400 border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase" href="https://donorbox.org/support-insight-collaborative" target="_blank">
<span>make a donation</span>
</a>
</div>
</section>
<?php get_footer(); ?>