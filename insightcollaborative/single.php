
<?php get_header(); ?>
<section <?php post_class(); ?>>
	<div class="container mx-auto pt-4 py-3 sm:pt-6">
		<a href="../../fellows/" class="flex-inline items-center"> 
			<object class="mr-1" data="/assets/arrow-blue-prev.svg" type="image/svg+xml"></object>
		 The Fellows' Blog</a>
	</div>

	<div class="container mx-auto pt-0 py-3">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php the_title( '<h1 class="text-h3 m-0 w-full text-center serif blue-400 mb-1">', '</h1>' ); ?>
	<h3 class="m-0 mb-2 text-caption text-center uppercase w-full">
		<?php the_tags('WRITTEN BY '); ?>
		<br>
		<?php echo the_field('fellow_place'); ?>
	</h3>

	<div class="entry-content">
    <?php if ( has_post_thumbnail() ) : ?>
      <?php the_post_thumbnail(); ?>
    <?php endif; ?>
    <?php the_content(); ?>
  </div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>