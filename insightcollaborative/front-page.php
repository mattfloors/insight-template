<?php get_header(); ?>
<?php $theme_color = get_field('theme_color', 'option'); ?>


<section class="bg-<? echo $theme_color ?>-400 p-gutter">
	<div class="mx-auto container flex flex-column justify-end aos-init aos-animate" data-aos-duration="1500" data-aos="fade-down">
		<h2 class="  text-h1 serif white m-0 py-3 pt-6 sm:py-6 sm:pt-8 text-center sm:text-left font-normal">
			We are a global leadership advisory non-profit, challenging exceptional individuals and teams to perform at their best.
		</h2>
	</div>
</section>

<!--
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php the_content(); ?>
<?php endwhile; endif; ?>
-->
<!--
<section class="container mx-auto text-center py-3 aos-init aos-animate" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<h2 class="uppercase text-caption m-0 mb-2">What we do</h2>
<h3 class="text-h3 serif">Informed by our work with world-leading organizations, we share the best leadership and conflict management theory and skills with those who cannot usually access them, from NGO field staff to political officials.</h3>
</section>
-->
<!-- class="bg-<?php echo $theme_color ?>-400" -->
<!-- <section>
  <div class="container mx-auto py-4 pb-0 flex flex-wrap">
    <figure class="w-full sm:w-1/2 p-gutter relative mb-2 sm:mb-0 aos-init aos-animate" data-aos="fade-up" data-aos-delay="500" data-aos-duration="2000">
      <img src="/img/top.jpg" class="z-20 responsive relative">
      <img src="/img/bg-top.jpg" class="z-10 absolute responsive parallax-image--left" style="transition: transform 0.4s cubic-bezier(0, 0, 0, 1) 0s; will-change: transform; transform: translate3d(0px, -3px, 0px);">
    </figure>
    <div class="sm:w-5/12 p-gutter sm:ml-auto aos-init aos-animate" data-aos="fade-up" data-aos-delay="250" data-aos-duration="2000">
      <h2 class="uppercase text-caption m-1 mb-2 mt-2 sm:mt-0">what we do</h2>
      <p class="text-h3 serif">Informed by our work with world-leading organizations, we share the best leadership and conflict management theory and skills with those who cannot usually access them, from NGO field staff to political officials.</p>
    </div>
  
  </div>
</section> -->
<section class="py-4 pb-0">
  <div class="container mx-auto flex flex-wrap">
    <figure class="w-full sm:w-1/2 p-gutter relative mb-2 sm:mb-0 aos-animate" data-aos="fade-up" data-aos-delay="500" data-aos-duration="2000">
      <img src="/img/top.jpg" class="z-20 responsive relative">
      <img src="/img/bg-top.jpg" class="z-10 absolute responsive parallax-image--left">
    </figure>
    <div class="sm:w-5/12 p-gutter sm:ml-auto aos-init aos-animate" data-aos="fade-up" data-aos-delay="250" data-aos-duration="2000">
      <h2 class="uppercase text-caption m-0 mb-2">what we do</h2>
      <p class="text-h3 serif">Informed by our work with world-leading organizations, we share the best leadership and conflict management theory and skills with those who cannot usually access them, from NGO field staff to political officials.
      </p>
      
    </div>
  
  </div>
</section>
<!-- <p class="serif text-h3 m-0 mb-3 white sm:w-11/12 mx-auto">We have helped individuals, non-profits and governments in over 60 countries to have more impact.</p> -->
<section >
  <div class="container mx-auto text-center py-2 sm:py-4">

    
    <ul class="flex flex-wrap list-none m-0 p-0">
      <li class="w-full mx-auto sm:w-1/5 p-gutter mb-1">
        <a href="http://insightcollaborative.org/what-we-do/#grouptraining" class="flex sm:flex-column items-center <?php echo $theme_color ?>-400 no-underline h-full mb-1 sm:mb-0">
          <figure class="h-3 w-3 relative sm:w-5/6 sm:h-auto flex align-center justify-center" >
            <img src="/img/ic-group-training.svg" class="w-3/4 inset-center">
          </figure>
          <hr class="<?php echo $theme_color ?>-400 w-0 h-full sm:h-0 sm:w-full my-1 border-1">
          <h3 class="w-1/3 sm:w-auto text-p font-normal m-0 sm:mb-1 text-left sm:text-center">Group Training</h3>
          <span class="sm:mt-auto">
            <span class="hover:arrow-animation">
              <span><object data="/assets/long-arrow-next-<?php echo $theme_color ?>.svg" type="image/svg+xml"></object></span>
            </span>
          </span>
        </a>
      </li>
      <li class="w-full mx-auto sm:w-1/5 p-gutter mb-1">
        <a href="http://insightcollaborative.org/what-we-do/#coachingmentoring" class="flex sm:flex-column items-center <?php echo $theme_color ?>-400 no-underline h-full">
          <figure class="h-3 w-3 relative sm:w-5/6 sm:h-auto flex align-center justify-center">
            <img src="/img/ic-coaching.svg" class="w-3/4 inset-center">
          </figure>
          <hr class="<?php echo $theme_color ?>-400 w-0 h-full sm:h-0 sm:w-full my-1 border-1">
          <h3 class="w-1/3 sm:w-auto text-p font-normal m-0 sm:mb-1 text-left sm:text-center">Coaching</h3>
          <span class="sm:mt-auto">
            <span class="hover:arrow-animation">
              <span><object data="/assets/long-arrow-next-<?php echo $theme_color ?>.svg" type="image/svg+xml"></object></span>
            </span>
          </span>
        </a>
      </li>
      <li class="w-full mx-auto sm:w-1/5 p-gutter mb-1">
        <a href="http://insightcollaborative.org/what-we-do/#cultureshaping" class="flex sm:flex-column items-center <?php echo $theme_color ?>-400 no-underline h-full">
          <figure class="h-3 w-3 relative sm:w-5/6 sm:h-auto flex align-center justify-center">
            <img src="/img/ic-culture-shaping.svg" class="w-3/4 inset-center">
          </figure>
          <hr class="<?php echo $theme_color ?>-400 w-0 h-full sm:h-0 sm:w-full my-1 border-1">
          <h3 class="w-1/3 sm:w-auto text-p font-normal m-0 sm:mb-1 text-left sm:text-center">Culture Shaping</h3>
          <span class="sm:mt-auto">
            <span class="hover:arrow-animation">
              <span><object data="/assets/long-arrow-next-<?php echo $theme_color ?>.svg" type="image/svg+xml"></object></span>
            </span>
          </span>
        </a>
      </li>
      <li class="w-full mx-auto sm:w-1/5 p-gutter mb-1">
        <a href="http://insightcollaborative.org/what-we-do/#mediation" class="flex sm:flex-column items-center <?php echo $theme_color ?>-400 no-underline h-full">
          <figure class="h-3 w-3 relative sm:w-5/6 sm:h-auto">
            <img src="/img/ic-mediation.svg" class="w-3/4 inset-center">
          </figure>
          <hr class="<?php echo $theme_color ?>-400 w-0 h-full sm:h-0 sm:w-full my-1 border-1">
          <h3 class="w-1/3 sm:w-auto text-p font-normal m-0 sm:mb-1 text-left sm:text-center">Mediation</h3>
          <span class="sm:mt-auto">
            <span class="hover:arrow-animation">
              <span><object data="/assets/long-arrow-next-<?php echo $theme_color ?>.svg" type="image/svg+xml"></object></span>
            </span>
          </span>
        </a>
      </li>
      <li class="w-full mx-auto sm:w-1/5 p-gutter mb-1">
        <a href="http://insightcollaborative.org/what-we-do/#meetingfacilitation" class="flex sm:flex-column items-center <?php echo $theme_color ?>-400 no-underline h-full">
          <figure class="h-3 w-3 relative sm:w-5/6 sm:h-auto">
            <img src="/img/ic-meeting-facilitation.svg" class="w-3/4 inset-center">
          </figure>
          <hr class="<?php echo $theme_color ?>-400 w-0 h-full sm:h-0 sm:w-full my-1 border-1">
          <h3 class="w-1/3 sm:w-auto text-p font-normal m-0 sm:mb-1 text-left sm:text-center">Meeting Facilitation</h3>
          <span class="sm:mt-auto">
            <span class="hover:arrow-animation">
              <span><object data="/assets/long-arrow-next-<?php echo $theme_color ?>.svg" type="image/svg+xml"></object></span>
            </span>
          </span>
        </a>
      </li>
    </ul>
  </div>
</section>

<section class="py-3 pt-0">
<div class="container mx-auto">
<h2 class="text-h3 m-0 mb-3 text-center serif">
Through our fellowships and projects, we enable individuals to pursue their interests and strengthen their skills in conflict management around the world.
</h2>
<div class="flex flex-wrap">
<div class="sm:w-1/2 w-full p-gutter">
<article class="bg-white px-1 sm:px-3 pb-3 mt-2 mb-2 sm:mb-0">
<figure class="-ml-3 relative -top-2">
<img class="w-full" src="/img/FELLOWSHIP.jpg">
</figure>
<h2 class="uppercase text-caption">fellowships</h2>
<div class="text-p"><p>Our Fellowships support talented individuals as they develop into the next generation of peacemakers and global citizens.</p></div>
<a class="blue-400 no-underline flex-inline items-center font-medium text-c" href="http://insightcollaborative.org/program/">Learn more
<span class="hover:arrow-animation ml-1 flex-inline items-center" rel="next">
<span>
<object data="/assets/long-arrow-next-blue.svg" type="image/svg+xml"></object>
</span>
</span>
</a></article>

</div>
<div class="sm:w-1/2 w-full p-gutter">
<article class="bg-white px-1 sm:px-3 pb-3 mt-2 mb-2 sm:mb-0">
<figure class="-ml-3 relative -top-2">
<img class="w-full" src="/img/PEACE.jpg">
</figure>
<h2 class="uppercase text-caption">projects</h2>
<div class="text-p"><p>Our projects help us reach underserved populations through collaborations with other individuals and organizations.</p></div>
<a class="blue-400 no-underline flex-inline items-center font-medium text-c" href="http://insightcollaborative.org/projects/">Learn more
<span class="hover:arrow-animation ml-1 flex-inline items-center" rel="next">
<span>
<object data="/assets/long-arrow-next-blue.svg" type="image/svg+xml"></object>
</span>
</span>
</a></article>

</div>
</div>
</div>
</section>

<?php if( have_rows('quotes') ): //parent group field

?>
<section class="sm:py-4 <?php echo $theme_color ?>" >
  <div data-js-carousel class="container mx-auto overflow-hidden" data-aos="fade-down" >
<?php
  while( have_rows('quotes') ): the_row();

  $quote_text = get_sub_field('quote_text');
  $quote_author = get_sub_field('quote_author');
  $quotecontent = get_sub_field('quotecontent');
?>

<article data-js-carousel-item>
      <div class="pl-2 relative border-t-2 border-b-2 h-full border-<?php echo $theme_color ?>-400 flex flex-column items-start">
        <img src="/assets/blockquote-<?php echo $theme_color ?>.svg" class="absolute top-1 left-0">
        <div class="text-p mt-2" data-js-carousel-title><?php echo $quote_text ?></div>

        <div aria-hidden="true" data-js-carousel-content>
          <div class="content text-p pt-2 sm:w-10/12">
            <blockquote class="text-blockquote m-0"><?php echo $quote_text ?></blockquote>
            <p>
              <?php echo $quotecontent ?>
            </p>
            <p class="text-c">
              <?php echo $quote_author ?>
            </p>
          </div>
        </div>
        
        <button data-js-carousel-toggle type="button" class="pt-0 py-2 border-0 background-none p-0 <?php echo $theme_color ?>-400 flex-inline items-center text-c mt-auto <?php echo $theme_color ?>-400 font-medium">Read&nbsp;<span js-close>more</span><span js-open>less</span> <span class="active:arrow-rotate"></span></button>
      </div>
    </article>


<?php
  endwhile;

?>
  </div>
</section>
<?php
  endif;
?>



<section class="background-cover" style="background-image: url(/img/bg-section.jpg);">
  <article class="mx-auto container flex flex-wrap py-2 sm:py-4 aos-init aos-animate" data-aos-duration="1500" data-aos="fade-up" data-aos-delay="500">
    <figure class="mb-4 sm:mb-0 sm:w-5/12 p-gutter relative aos-init aos-animate" data-aos="fade-up" data-aos-delay="250" data-aos-duration="2000">
      <img src="/img/homepage.jpg" class="z-20 w-full relative">
      <img src="/img/bg_homepage.jpg" class="z-10 absolute responsive opacity-08 parallax-image--left" style="transition: transform 0.4s cubic-bezier(0, 0, 0, 1) 0s; will-change: transform; transform: translate3d(0px, 108px, 0px);">
    </figure>
    <div class="w-full sm:w-1/2 sm:ml-auto p-gutter aos-init aos-animate" data-aos-duration="2000" data-aos="fade-up" data-aos-delay="500">
      <h2 class="uppercase text-caption m-0 mb-2">Who we are</h2>
      <p class="text-h3 serif">We built our reputation as conflict management consultants and now use our negotiation and mediation expertise to create environments where individuals can thrive, from top executive teams to underprivileged communities.</p>
      <footer>
        <a href="http://insightcollaborative.org/who-we-are/" class="<?php echo $theme_color ?>-400 no-underline flex-inline items-center font-medium text-c">
          Read more
          <span class="hover:arrow-animation ml-1 flex-inline items-center" rel="next">
            <span><object data="/assets/long-arrow-next-<?php echo $theme_color ?>.svg" type="image/svg+xml"></object></span>
          </span>
        </a>
      </footer>
    </div>
  </article>
</section>
<?php if( have_rows('support_us') ): //parent group field
  while( have_rows('support_us') ): the_row();
  $support_us_text = get_sub_field('support_us_text');
  $support_us_button_text = get_sub_field('support_us_button_text');
  $support_us_button_url = get_sub_field('support_us_button_url');
  $support_us_button_behaviour = get_sub_field('support_us_button_behaviour');
?>

<section class="bg-white black px-2 sm:px-0 text-center">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init aos-animate" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<h4 class="uppercase text-caption m-0">Support us</h4>
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6"><?php echo $support_us_text ?></p>
<a class="blue-400 border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase" href="https://donorbox.org/support-insight-collaborative" target="<?php echo $support_us_button_behaviour ?>">
<span><?php echo $support_us_button_text ?></span>
</a>
</div>
</section>

<?php
  endwhile;
  endif;
?>
<section class="bg-green-400 px-2 sm:px-0 text-center white">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init aos-animate" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<img alt="Insight Collaborative Online" class="responsive" src="/assets/IP_LOGO_MAIN_NEGATIVE.svg">
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6">Our sister organization Insight Partners is a global leadership advisory firm, challenging exceptional individuals and teams to be their best.</p>
<a class="border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase white" href="https://insightpartnersonline.com/" target="_blank">
<span>VISIT the webSITE</span>
</a>
</div>
</section>

<?php get_footer(); ?>