<?php /* Template Name: Fellow Template */ ?>


<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>
	<div class="entry-links"><?php wp_link_pages(); ?></div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>