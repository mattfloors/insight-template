<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />


<meta charset="utf-8" content="text/html" http-equiv="content-type">
<meta content="ie=edge,chrome=1" http-equiv="x-ua-compatible">
<meta content="" name="description">
<meta content="" name="author">
<meta charset="UTF-8">
<meta content="telephone=no" name="format-detection">
<link href="/dev/favicon-ip.png" rel="icon" type="image/png">
<meta content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width" name="viewport">
<script crossorigin="anonymous" defer="" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script defer="" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script defer="" src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script defer="" src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.0.2/dist/simpleParallax.min.js"></script>
<script defer="" src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script><link as="style" href="https://unpkg.com/aos@2.3.1/dist/aos.css" onload="this.onload=null;this.rel='stylesheet'" rel="stylesheet"><noscript>

</noscript>
<link href='https://unpkg.com/aos@2.3.1/dist/aos.css' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"></noscript>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">

<meta name="viewport" content="width=device-width" />
<?php 
	
  $theme_colors_grey = get_field('theme_colors_grey', 'option');
  $theme_colors_green = get_field('theme_colors_green', 'option');
  $theme_colors_blue = get_field('theme_colors_blue', 'option');
  $theme_color = get_field('theme_color', 'option');

  wp_head(); 

	
?>

<style>
  @font-face {
  	font-family: 'Untitled Sans Web';
  	src: url('/dev/assets/UntitledSansWeb-Regular.woff') format('woff');
  	font-weight:  400;
  	font-style:   normal;
  	font-stretch: normal;
  }
  
  @font-face {
  	font-family: 'Untitled Sans Web';
  	src: url('/dev/assets/UntitledSansWeb-RegularItalic.woff') format('woff');
  	font-weight:  400;
  	font-style:   italic;
  	font-stretch: normal;
  }
  
  @font-face {
  	font-family: 'Untitled Sans Web';
  	src: url('/dev/assets/UntitledSansWeb-Medium.woff') format('woff');
  	font-weight:  500;
  	font-style:   normal;
  	font-stretch: normal;
  }
  
  @font-face {
  	font-family: 'Untitled Sans Web';
  	src: url('/dev/assets/UntitledSansWeb-MediumItalic.woff') format('woff');
  	font-weight:  500;
  	font-style:   italic;
  	font-stretch: normal;
  }
  
  @font-face {
  	font-family: 'Canela Web';
  	src: url('/dev/assets/Canela-Regular-Web.woff') format('woff');
  	font-weight:  400;
  	font-style:   normal;
  	font-stretch: normal;
  }
  
  :root {
  	--color-grey: <?php echo $theme_colors_grey ?>;
  	--color-green: <?php echo $theme_colors_green ?>;
  	--color-blue: <?php echo $theme_colors_blue ?>;
  	}

</style>
</head>
<body <?php body_class(); ?>>
  <header class="absolute top-0 left-0 w-full z-30 overflow-hidden">
    <div class="mx-auto flex flex-wrap sm:container md:container lg:container sm:flex-no-wrap justify-between items-center <?php echo $theme_color ?>">

    <!--  <?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '<h1 class="ml-1 sm:ml-0">'; } ?> -->


      <h1 class="ml-1 sm:ml-0 w-full">
        
        <a style="position: relative; top: 2px" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
          
          <?php 
            if ( has_custom_logo() ) :
              $custom_logo_id = get_theme_mod( 'custom_logo' );
              $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
              $logo_url = $logo[0];
              
          ?>
            <!--<img src="<?php echo $logo_url ?>" alt="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>">-->
            <img alt='Insight partners Online Logo' 
              height="37"
              class='has-navigation-closed sm:w-auto w-3/4' 
              src='/dev/assets/IP_LOGO_NEGATIVE@1.5x.svg'>
            <img alt='Insight partners Online Logo' 
              class='is-homepage-scroll sm:w-auto w-3/4'
              height="37"
              src='/dev/assets/IP_LOGO_POSITIVE@1.5x.svg'>

          <?php 
            endif;
          ?>

        </a>

      </h1>

      <!-- <?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '</h1>'; } ?> -->

    
      <?php wp_nav_menu( array( 
          'theme_location' => 'main-menu',
          'container' => 'nav',
          'container_class' => 'flex flex-column ' . $theme_color . ' sm:flex-row w-full justify-center sm:h-auto sm:w-auto text-center' ) 
        ); 
      ?>

      <button aria-hidden="true" class="border-0 background-none absolute toggle-btn">
        <div class="hamburger hamburger--spin js-hamburger">
          <div class="hamburger-box">
            <div class="hamburger-inner"></div>
          </div>
        </div>
      </button>
    </div>
  </header>


<main>