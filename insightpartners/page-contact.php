<?php /* Template Name: Contact Template */ ?>

<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<section style="background-image: url(/dev/img/bg-contact-us.jpg);">
  <div class="py-2 sm:py-4 pt-4 sm:pt-8 background-cover">
    <div class="flex flex-wrap container mx-auto">
      <h2 class="w-full mt-0 mb-4 text-h3 serif text-center">
        Get in touch to find out more about our work.
      </h2>
    </div>
    <div class="flex flex-wrap container mx-auto">
      <?php the_content(); ?>
    </div>
  </div>
</section>

<section class="bg-blue-400 px-2 sm:px-0 text-center white">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init aos-animate" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<img alt="Insight Collaborative Online" class="responsive" src="/dev/assets/IC_LOGO_MAIN@1x.svg">
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6">Through our non-profit sister organization, we share the same insights and skills with underserved groups around the world.</p>
<a class="border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase white" href="https://insightcollaborative.org/" target="_blank">
<span>VISIT the webSITE</span>
</a>
</div>
</section>

<?php endwhile; endif; ?>

<?php get_footer(); ?>