<?php /* Template Name: Partner Parent Template */ ?>
<?php $theme_color = get_field('theme_color', 'option'); ?>


<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php the_content(); ?>


<div class="container mx-auto flex-wrap flex py-3">
<h2 class="uppercase text-caption m-0 mb-3 w-full text-center">Meet The partners</h2>




<?php

	$parentID = get_the_ID();

$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
 );




$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : ?>

    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

		<?php $currentID = get_the_ID(); ?>

        <article class="sm:w-1/3 p-gutter sm:mt-0 -mt-2px">
			<div class="pl-2 relative border-t-2 border-b-2 h-full border-<?php echo $theme_color ?>-400 flex flex-column items-start">
			<img class="absolute top-1 left-0" src="/dev/assets/blockquote-<?php echo $theme_color ?>.svg">
			<div class="text-p mt-2"><?php echo get_field('partner_introduction', $currentID) ?></div>
			<p class="text-c mb-0"><?php the_title(); ?><br>
				<?php echo get_field('partner_role', $currentID) ?></p>
			<a class="text-c py-2 mt-auto <?php echo $theme_color ?>-400 flex-inline items-center no-underline font-medium" href="<?php echo get_page_link($currentID) ?>">
				<?php $partner_fullname = get_the_title(); 
					$partner_name = explode(' ', trim($partner_fullname) )[0]
				?>
			Hear more<br> from <?php echo $partner_name ?>
			<span class="hover:arrow-animation ml-1">
			<span>
			<object data="/dev/assets/long-arrow-next-<?php echo $theme_color ?>.svg" type="image/svg+xml"></object>
			</span>
			</span>
			</a>
			</div>
		</article>

    <?php endwhile; ?>

<?php endif; wp_reset_postdata(); ?>
</div>
</article>
<?php endwhile; endif; ?>
</div>


<section class="bg-blue-400 px-2 sm:px-0 text-center white">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init aos-animate" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<img alt="Insight Collaborative Online" class="responsive" src="/dev/assets/IC_LOGO_MAIN@1x.svg">
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6">Through our non-profit sister organization, we share the same insights and skills with underserved groups around the world.</p>
<a class="border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase white" href="https://insightcollaborative.org/" target="_blank">
<span>VISIT the webSITE</span>
</a>
</div>
</section>

<?php get_footer(); ?>