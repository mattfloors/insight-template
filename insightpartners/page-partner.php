<?php /* Template Name: Partner Template */ ?>
<?php $theme_color = get_field('theme_color', 'option'); ?>

<?php get_header(); ?>
<main>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php /*post_class();*/ ?>
<section id="post-<?php the_ID(); ?>" 
		 class="background-cover" 
		 style="background-image: url(/dev/img/bg-hp-what-we-do.jpg);">
	<div class="container mx-auto py-3 pt-6 sm:py-6 sm:pt-8">
		<div class="sm:w-3/4 p-gutter aos-init aos-animate" 
			 data-aos-delay="500" 
			 data-aos-duration="2000" 
			 data-aos="fade-up">
			<?php edit_post_link(); ?>
			<h1 class="text-h3 m-0 mb-2 serif"><?php the_title(); ?>
				<span class="block grey-400"><?php echo get_field('partner_role') ?></span>
			</h1>
			<div class="text-p"><?php echo get_field('partner_excerpt') ?></div>
		</div>
	</div>
</section>

<?php the_content(); ?>

<section class="relative bg-white aos-init aos-animate" 
		 data-aos-delay="500"
		 data-aos-duration="2000"
		 data-aos="fade-up">
	<div class="container mx-auto flex-wrap flex py-3 justify-center">
		<h2 class="uppercase text-caption m-0 mb-3 w-full text-center">Meet The partners</h2>
	<?php 
		$postID = $post->ID;
		$parents = get_ancestors( $post->ID, 'page', 'post_type' );
	    $parentID = array_values($parents)[0];

	    

	    if($parentID) :


			$args = array(
			    'post_type'      => 'page',
			    'posts_per_page' => -1,
			    'post_parent'    => $parentID,
			    'order'          => 'ASC',
			    'orderby'        => 'menu_order'
			);


			$parent = new WP_Query( $args );

			if ( $parent->have_posts() ) : ?>

    			<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

					<?php  $currentID = get_the_ID();?>

					<?php if ($postID !== $currentID ) : ?>
						<div class="sm:w-1/3 -mt-2px sm:mt-0">
					        <article class="h-full p-gutter sm:mt-0 -mt-2px">
							<div class="pl-2 relative border-t-2 border-b-2 h-full border-<?php echo $theme_color ?>-400 flex flex-column items-start">
							<img class="absolute top-1 left-0" src="/dev/assets/blockquote-<?php echo $theme_color ?>.svg">
							<div class="text-p mt-2"><?php echo get_field('partner_introduction', $currentID) ?></div>
							<p class="text-c mb-0"><?php the_title(); ?><br>
								<?php echo get_field('partner_role', $currentID) ?></p>
							<a class="text-c py-2 mt-auto <?php echo $theme_color ?>-400 flex-inline items-center no-underline font-medium" href="<?php echo get_page_link($currentID) ?>">
								<?php $partner_fullname = get_the_title(); 
									$partner_name = explode(' ', trim($partner_fullname) )[0]
								?>
							Hear more<br> from <?php echo $partner_name ?>
							<span class="hover:arrow-animation ml-1">
							<span>
							<object data="/dev/assets/long-arrow-next-<?php echo $theme_color ?>.svg" type="image/svg+xml"></object>
							</span>
							</span>
							</a>
							</div>
							</article>
						</div>					
    			<?php endif; endwhile; ?>

			<?php endif; wp_reset_postdata(); ?>

	    <?php endif; ?>
	    </div>
	</div>
</section>



<?php endwhile; endif; ?>
<section class="bg-blue-400 px-2 sm:px-0 text-center white">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init aos-animate" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<img alt="Insight Collaborative Online" class="responsive" src="/dev/assets/IC_LOGO_MAIN@1x.svg">
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6">Through our non-profit sister organization, we share the same insights and skills with underserved groups around the world.</p>
<a class="border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase white" href="https://insightcollaborative.org/" target="_blank">
<span>VISIT the webSITE</span>
</a>
</div>
</section>
</main>
<?php get_footer(); ?>