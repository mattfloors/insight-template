<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!--
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="header">
	<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
</header>
<div class="entry-content">
<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
-->
<?php the_content(); ?>
<!--
<div class="entry-links"><?php wp_link_pages(); ?></div>
</div>
</article>
-->


<?php
				if( have_rows('accordion_repeater') ): ?>
					<section class="p-gutter py-2 ic-theme">
<h2 class="text-caption uppercase text-center m-0 mb-2">
faq
</h2>
					<div class="container mx-auto">
						<div class="border-t-2 border-blue-400 accordion-js-init" data-js-accordion="">


					<?php 

					// loop through rows (parent repeater)
					while( have_rows('accordion_repeater') ): the_row(); ?>
						<?php
							$accordion_title = get_sub_field('accordion_title');
							$accordion_excerpt = get_sub_field('accordion_excerpt');
							$accordion_content = get_sub_field('accordion_content');
							$accordion_image = get_sub_field('accordion_image');
						?>


						<div class="border-b-2 border-blue-400" data-js-accordion-item="" id="item-5">
							<h2 class="flex justify-between" data-js-accordion-button="">
								<span class="serif"><?php echo $accordion_title; ?></span>
								<button class="ml-auto border-0 background-none" type="button"><span class="active:arrow-rotate--big mr-1"></span></button>
							</h2>
							<div data-js-accordion-content="">
							<div class="text-c pt-0 pb-1 sm:w-10/12">
								<?php echo $accordion_content; ?>
							</div>
							</div>
						</div>	

					<?php endwhile; // while( has_sub_field('to-do_lists') ): ?>
					</div></div></section>
				<?php endif; // if( get_field('to-do_lists') ): ?>

			


<?php endwhile; endif; ?>

<?php get_footer(); ?>