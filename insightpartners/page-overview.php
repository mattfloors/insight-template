<?php /* Template Name: Partner Overview */ ?>

<?php get_header(); ?>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <?php the_content(); ?>

  <section style="background-image: url(/img/bg-hp-what-we-do.jpg);" class="background-cover">
  <div class="container mx-auto py-2 sm:py-8 pt-6 sm:pt-8 flex flex-wrap">
    <figure class="w-full sm:w-1/2 p-gutter mb-2 sm:mb-0 relative sm:order-last aos-init aos-animate" data-aos="fade-up" data-aos-delay="500" data-aos-duration="2000">
      <img src="/img/fellows_front.jpg" class="z-20 responsive relative">
      <img src="/img/fellows_back.jpg" class="z-10 absolute responsive parallax-image--left" style="transition: transform 0.4s cubic-bezier(0, 0, 0, 1) 0s; will-change: transform; transform: translate3d(0px, 3px, 0px);">
    </figure>
    <div class="sm:w-5/12 p-gutter sm:mr-auto sm:order-first aos-init aos-animate" data-aos="fade-up" data-aos-delay="250" data-aos-duration="2000">
      <h2 class="uppercase text-caption m-1 mb-2">overview</h2>
      <p class="text-h3 serif">The Insight Fellowship Program helps exceptional individuals develop their own conflict management skills by working within local or international non-profits of their choice. </p>
      <div class="text-p">
        <p>
          Each year-long Fellowship begins with a period of training and development before the fellow begins three global placements through which they share the best in conflict management theory and skills. 
        </p>
        <p>
          The community of Insight Fellows has made invaluable contributions to non-profits and the people they serve in almost 40 countries, including working with communities affected by some of the world’s most violent conflicts. 
        </p>

        <a href="http://mattfloors.site/wordpress/principles-and-faq/" class="blue-400 no-underline flex-inline items-center font-medium text-c">
          Learn more
          <span class="hover:arrow-animation ml-1 flex-inline items-center" rel="next">
            <span><object data="/assets/long-arrow-next-blue.svg" type="image/svg+xml"></object></span>
          </span>
        </a>
        <br>
        <a href="http://mattfloors.site/wordpress/how-to-apply/" class="blue-400 border-2 mt-1 flex-inline items-center lh-3 ls-custom no-underline px-2 space-no-wrap text-c uppercase">apply</a>
      </div>
    </div>
  
  </div>
</section>

<section class="sm:py-4 container mx-auto ic-theme">
  <h2 class="text-h3 text-center blue-400 serif my-2 sm:mt-0">Insight Fellows gain formative skills and experiences, while helping organizations across the globe work more effectively:</h2>
  <div data-js-carousel="" class="overflow-hidden blue" data-aos="fade-up" data-aos-delay="250" data-aos-duration="2000">


      <article data-js-carousel-item="" style="width: 100%; display: inline-block;">
      <div class="px-2 relative border-t-1 border-b-1 h-full border-blue-400 flex flex-column items-start">

        <div class="font-medium mt-2">Kartik Madiraju</div>
        <img src="/assets/blockquote-blue.svg" class="absolute top-3 left-0">
        <div class="text-p mt-1" data-js-carousel-title="">The Insight Fellowship was unique in the way it gave me the freedom to pursue my interests while gaining the skills and experience I needed to grow.</div>

        <div aria-hidden="true" data-js-carousel-content="">
          <div class="content text-p pt-2 sm:w-10/12">
            <blockquote class="text-blockquote m-0">The Insight Fellowship was unique in the way it gave me the freedom to pursue my interests while gaining the skills and experience I needed to grow.</blockquote>
            <p>Before I pursued my goal to become an attorney, I was on the lookout for an opportunity which would help me learn about negotiation and peacebuilding while also complimenting my passion for the environment. The Insight Fellowship was unique in the way it gave me the freedom to pursue these interests while gaining the skills and experience I needed to grow personally and professionally.</p>
            <p>The highlight of my year had to be working with EcoPeace Middle East, an organisation using environmental conservation as a tool for peacebuilding. I worked as a trainer and mentor for all of the 50 staff members, and the role also allowed me to travel across Jordan, Palestine and Israel. I learned that people have so much more in common than what separates them and if we can meet certain needs we can have hope for peace. </p>
            <p>The Fellowship offered training and mentorship but also the space and freedom to form and pursue my own ideas. Overall, it taught me to be mindful, and understand that listening carefully to different people’s perspectives is essential to moving forward in negotiations. If it wasn’t for the Fellowship, I wouldn’t be the person I am today.</p>
          </div>
        </div>
        
        <button data-js-carousel-toggle="" type="button" class="py-2 border-0 ic-theme background-none p-0 blue-400 flex-inline items-center text-c mt-auto blue-400 font-medium" tabindex="0">Read&nbsp;<span js-close="">more</span><span js-open="">less</span> <span class="active:arrow-rotate"></span></button>
      </div>
    </article>






    <article data-js-carousel-item="" style="width: 100%; display: inline-block;">
      <div class="px-2 relative border-t-1 border-b-1 h-full border-blue-400 flex flex-column items-start">

      <div class="font-medium mt-2">Elise Willer</div>

        
        <img src="/assets/blockquote-blue.svg" class="absolute top-3 left-0">
        <div class="text-p mt-1" data-js-carousel-title="">The Fellowship has been a fantastic springboard into a career in conflict management.<br><br><br><br></div>

        <div aria-hidden="true" data-js-carousel-content="">
        <div class="content text-p pt-2 sm:w-10/12">
          <blockquote class="text-blockquote m-0">The Fellowship has been a fantastic springboard into a career in conflict management. </blockquote>
          <p>
            From application to final presentation, the Insight Fellowship pushed me to think harder about the impact I wanted to have during the Fellowship and beyond. As a Fellow, I travelled to Ghana, Uganda, Rwanda, Kenya and Peru, supporting a variety of NGOs, social enterprises and charities as they tackled obstacles, learned to understand each other better and amplified their impact on the world. For example, in Ghana I helped employees at Saha Global improve their communication skills, manage cultural differences, and encourage behaviour change in rural villages which paved the way for communities to adopt safer methods of treating and drinking water. </p>

          <p> Additionally, the Fellowship was a unique opportunity for personal growth. The guidance and mentorship I received from the Insight team throughout my Fellowship was invaluable. The team’s belief in me even when I doubted my abilities created a strong foundation for reflection and then David and Patrick were able to push me to grapple with who I am, and what I want for my life in a way that I had never done before. </p>

          <p> The Fellowship has been a fantastic springboard into a career in conflict management. I feel honored and delighted to be a member of the Insight family, and to play a role in efforts to improve relationships across the globe.

                      </p>
                      
                    </div>
                  </div>

        <button data-js-carousel-toggle="" type="button" class="py-2 border-0 ic-theme background-none p-0 blue-400 flex-inline items-center text-c mt-auto blue-400 font-medium" tabindex="0">Read&nbsp;<span js-close="">more</span><span js-open="">less</span> <span class="active:arrow-rotate"></span></button>
      </div>
    </article>
    <article data-js-carousel-item="" style="width: 100%; display: inline-block;">
      <div class="px-2 relative border-t-1 border-b-1 h-full border-blue-400 flex flex-column items-start">
        <img src="/assets/blockquote-blue.svg" class="absolute top-3 left-0">
        <div class="font-medium mt-2">Carl Conradi</div>
        <div class="text-p mt-1" data-js-carousel-title="">The workshop proved so successful that my co-facilitator has continued to run it with hundreds more women and young people across the region in the years since.</div>

        <div aria-hidden="true" data-js-carousel-content="">
        <div class="content text-p pt-2 sm:w-10/12">
          <blockquote class="text-blockquote m-0">The workshop proved so successful that my co-facilitator has continued to run it with hundreds more women and young people across the region in the years since.</blockquote>
          <p>
            The workshop proved so successful that my co-facilitator has continued to run it with hundreds more women and young people across the region in the years since.</p>

          <p>The Insight Fellowship allowed me to pursue my interest in children in conflict through placements at the International Criminal Court in The Hague, in Somaliland and in the Democratic Republic of Congo (DRC). In Somaliland, young people often feel alienated from the elder-driven democratic process and struggle to have their grievances and interests taken seriously. Working with a local co-facilitator, I translated (linguistically and culturally) the materials I’d learned in the US in order to run a workshop for young adults to improve their communication skills, both amongst each other and with leaders. The workshop proved so successful that my co-facilitator has continued to run it with hundreds more women and young people across the region in the years since. </p>

          <p>When working to understand disarmament communication strategies being directed towards active child soldiers in the DRC, I managed to help negotiate the release of a child from a Congolese army camp, who we then set on a journey to demobilisation. It was Insight’s teaching on how to exercise curiosity and ask the right questions which proved invaluable in this situation.</p>

          <p>After finishing the Fellowship, I went on to pursue an MA in Conflict, Security and Development at King’s College London, where I organised for a group of students to get similar vital field experience in Lebanon. From this, to subsequent work in Yemen, and returning to work with child soldiers in the DRC, I have been lucky to amplify the impact of my Fellowship by sharing my experiences across a wide range of different contexts.

          </p>
          
        </div>
      </div>

        <button data-js-carousel-toggle="" type="button" class="py-2 border-0 ic-theme background-none p-0 blue-400 flex-inline items-center text-c mt-auto blue-400 font-medium" tabindex="-1">Read&nbsp;<span js-close="">more</span><span js-open="">less</span> <span class="active:arrow-rotate"></span></button>
      </div>
    </article>

  </div>
</section>



<?php 

$args = array(
    'post_type' => 'fellows',
    'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => 9
);
$query = new WP_Query( $args ); 

  if ( $query->have_posts() ) : ?>

    <section class="bg-blue-400 py-2">
      <div class="container mx-auto white">
      <h2 class="text-caption uppercase text-center">
      the fellows' blog
      </h2>
      <h2 class="text-h3 mb-2 serif text-center">
      Follow our Fellows' journeys
      </h2>
      <p class="text-right">
        <a href="../fellows" class="no-underline white text-p mr-1 flex-inline items-center"> Read All Articles 
			<span class="hover:arrow-animation flex-inline items-center" style="width: 66px; ">
              <span><object data="/assets/long-arrow-next-white.svg" type="image/svg+xml"></object></span>
            </span>
        </a>
      </p>
      <div data-js-simple-carousel>

    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
        
        <article class="w-full sm:w-1/3 white p-gutter mb-1">
      <div class="p-2 sm:p-3 border-1 white">
        <div class="sm:-ml-1">
          <time><?php the_date(); ?></time>
          <a href="<?php the_permalink($post); ?>" class="no-underline white"> <?php the_title('<h2 class="serif">', '</h2>') ?></a>
        </div>
        
        <div class="text-p">
          <?php the_excerpt(); ?>
        </div>

        <?php if (has_post_thumbnail( $post->ID ) ): ?>
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
        ?>

        
        <div class="background-cover after-padding-66" style=" background-image: url(<?php echo $image[0];?>)"></div>
        <!--<div id="custom-bg" style="background-image: url('<?php echo $image[0]; ?>')">


        </div>-->
      <?php endif; ?>

        
      </div>
    </article>
    
    <?php endwhile; endif; ?>
  </div></div></section>
  <?php wp_reset_postdata(); ?>


<section class="bg-white black px-2 sm:px-0 text-center">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<h4 class="uppercase text-caption m-0">Support us</h4>
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6">Help us reach underserved groups and  communities with the tools they need to thrive. </p>
<a class="blue-400 border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase" href="https://donorbox.org/support-insight-collaborative" target="_blank">
<span>make a donation</span>
</a>
</div>
</section>

<section class="bg-green-400 px-2 sm:px-0 text-center white">
<div class="container mx-auto flex flex-column py-3 sm:py-4 items-center aos-init" data-aos-delay="500" data-aos-duration="2000" data-aos="fade-up">
<img alt="Insight Collaborative Online" class="responsive" src="/assets/IP_LOGO_MAIN_NEGATIVE.svg">
<p class="my-2 sm:my-3 text-h3 serif mx-auto md:w-5/6">Our sister organization Insight Partners is a global leadership advisory firm, challenging exceptional individuals and teams to be their best.</p>
<a class="border-2 flex-inline items-center lh-3 ls-custom no-underline px-1 sm:px-2 space-no-wrap text-c uppercase white" href="/inside_partner/" target="_blank">
<span>VISIT the website</span>
</a>
</div>
</section>

<?php endwhile; endif; ?>

<?php get_footer(); ?>